/**
 * Created by amilamanoj on 11/09/16.
 */
var Config = require('./config/config.js');
//var Config = require('./config/config.dev_local.js'); //for local 

/**
 * db connect
 */
var mongoose = require('mongoose');
mongoose.connect([Config.db.host, '/', Config.db.name].join(''),{
    //eventually it's a good idea to make this secure
    user: Config.db.user,
    pass: Config.db.pass
});

/**
 * create application
 */
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');


var app = express();

/**
 * app setup
 */
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

/**
 * routing
 */
var useCaseRoutes = require("./usecase/useCaseRoutes");

app.use('/api', useCaseRoutes());


module.exports = app;