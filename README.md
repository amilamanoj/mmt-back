# motius-mean-test Backend Bpplication
Motius MEAN stack test project.

## Prerequisites

* nodejs [official website](https://nodejs.org/en/) - nodejs includes [npm](https://www.npmjs.com/) (node package manager)
* mongoDB [official website](https://www.mongodb.com/)

## Setup (before first run)

* install mongodb and create `testdb`
* import usecase data to the collection `usecases`
* install npm dependencies `npm install`

## Running

* go to the project folder `cd to/your/folder`
* start http server `node server.js`
* with default configuration `localhost:3000` should be accessible

## Directory structure

and important files

```
app/                //test app
-- config/          // configuration
-- usecase/         // usecase module, includes mongoose schema and express.js routing
node_modules/       // npm modules
package.json        // npm dependencies information (this belongs into source control)
app.js              // application definition
server.js           // start script
```