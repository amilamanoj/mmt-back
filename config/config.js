var Config = {};
Config.db = {};
Config.app = {};
Config.auth = {};

Config.db.host = 'localhost';
Config.db.name = 'testdb';


// Use environment defined port or 3000
Config.app.port = process.env.PORT || 3000;

module.exports = Config;

