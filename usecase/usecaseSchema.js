// Load required packages
var mongoose = require('mongoose');

// Define our usecase schema
var Usecase   = new mongoose.Schema({
    title: String,
    // description: String,
    body: String
});

// Export the Mongoose model
module.exports = mongoose.model('Usecase', Usecase);