var Usecase = require('./usecaseSchema');

// endpoint /api/usecases for POST
exports.postUsecase = function (req, res) {

    var usecase = new Usecase(req.body);

    usecase.save(function (err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json(m);
    });
};

// endpoint /api/usecases for GET
exports.getUsecases = function (req, res) {
    console.log("Getting all usecases");
    Usecase.find(function (err, usecases) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(usecases);
    });
};


// endpoint /api/usecases/:usecase_id for GET
exports.getUsecase = function (req, res) {
    // Use the Usecase model to find a specific usecase
    Usecase.findById(req.params.usecase_id, function (err, usecase) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(usecase);
    });
};
