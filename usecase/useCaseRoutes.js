module.exports = useCaseRoutes;


function useCaseRoutes() {

    var usecaseController = require('./usecaseController');
    var router = require('express').Router();
    var unless = require('express-unless');

    router.route('/usecases')
        .post(usecaseController.postUsecase)    //create new usecase
        .get(usecaseController.getUsecases);    //retrieve all usecases

    router.route('/usecases/:usecase_id')
        .get(usecaseController.getUsecase);      //retrieve a specific usecase

    return router;
}
